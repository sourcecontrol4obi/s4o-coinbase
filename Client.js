"use strict";
var ClientBase = require('./ClientBase');

class Client extends ClientBase {

  /**
   * 
   * @param {Object} opts 
   * opts = {
    *   'apiKey'       : apyKey,
    *   'apiSecret'    : apySecret,
    *   'baseApiUri'   : baseApiUri
    * };
   */
  constructor(opts) {
    super(opts);
  }

  /**
   * 
   * @param {String} name account name
   * @param {Boolean} test is test account
   */
  async createAccount(name, test) {
    let promise = new Promise((resolve, reject) => {
      this._postHttp('/accounts', { name, test }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} id account id
   * @param {Object} update update body
   */
  async updateAccount(id, update) {
    let promise = new Promise((resolve, reject) => {
      this._putHttp(`/accounts/${id}`, update, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {Object} query 
   */
  async getAccounts(query) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/accounts', query, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} id account id
   * @param {Object} options additional query options
   */
  async getAccount(id, options) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp(`/accounts/${id}`, options, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
 * 
 * @param {Object} query 
 */
  async getWallets(query) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/wallets', query, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} id wallet id
   * @param {Object} options additional query options
   */
  async getWallet(id, options) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp(`/wallets/${id}`, options, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} account account id
   * @param {String} currency currency symbol
   * @param {String} label optional label for address
   */
  async generateAddress(account, currency, label = "") {
    let promise = new Promise((resolve, reject) => {
      this._postHttp('/addresses', { account, currency, label }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {Object} query
   */
  async getAddresses(query) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/addresses', query, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} account account id
   * @param {String} currency currency symbol
   * @param {{ String: String }} data transaction data { "addr": amount, ... }
   * @param {Number} fee fee to use for transaction (set to 0 for auto fee)
   * @param {String} comment comment on transaction
   * @param {String} key unique key for monitoring transaction
   */
  async sendTransaction(account, currency, data, fee, comment, key) {
    let promise = new Promise((resolve, reject) => {
      this._postHttp('/commands/sendMoney', { account, currency, data, fee, comment, key }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} account account id
   * @param {String} currency currency symbol
   * @param {String} hash   
   */
  async getTransaction(account, currency, hash) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp(`/commands/getTransaction`, { account, currency, id: hash }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} account account id
   * @param {String} currency currency symbol
   * @param {Number} count total count to return
   * @param {Number} skip total count to skip
   */
  async getTransactions(account, currency, count, skip) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/commands/getTransactions', { account, currency, count, skip }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} account account id
   * @param {String} currency currency symbol
   */
  async getBalance(account, currency) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/commands/getBalance', { account, currency }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} account account id
   * @param {String} currency currency symbol
   * @param {String} block block height
   */
  async getFee(account, currency, block) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/commands/getFee', { account, currency, block }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }
}

module.exports = Client;
