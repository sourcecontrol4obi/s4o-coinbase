"use strict";
var request = require('request'),
  Base = require('./Base'),
  crypto = require('crypto'),
  handleHttpError = require('./errorHandler').handleHttpError,
  _ = require('lodash'),
  qs = require('querystring'),
  assign = require('object-assign');

class ClientBase extends Base {
  //
  // constructor
  //
  // opts = {
  //   'apiKey'       : apyKey,
  //   'apiSecret'    : apySecret,
  //   'baseApiUri'   : baseApiUri
  // };
  constructor(opts) {
    super();
    // assign defaults and options to the context
    assign(this, {
      timeout: 5000
    }, opts);

    // check for the different auth strategies
    var api = !!(this.apiKey && this.apiSecret)

    if (!api) {
      throw new Error('you must provide "apiKey" & "apiSecret" parameters');
    }
  }

  //
  // private methods
  //
  _setAccessToken(path) {
    // OAuth access token
    if (this.accessToken) {
      if (path.indexOf('?') > -1) {
        return path + '&access_token=' + this.accessToken;
      }
      return path + '?access_token=' + this.accessToken;
    }
    return path;
  }

  _generateSignature(path, method, bodyStr) {
    var timestamp = Math.floor(Date.now() / 1000);
    var message = method + path + bodyStr;
    console.log(message);
    var signature = crypto.createHmac('sha256', this.apiSecret).update(message).digest('hex');

    return {
      'digest': signature,
      'timestamp': timestamp
    };
  };

  _generateReqOptions(url, path, body, method, headers) {

    var bodyStr = body ? JSON.stringify(body) : '';

    // specify the options
    var options = {
      'url': url,
      // 'ca': this.caFile,
      // 'strictSSL': this.strictSSL,
      'body': bodyStr,
      'method': method,
      'timeout': this.timeout,
      'headers': {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'coinaly/node/1.0.0'
      }
    };

    options.headers = assign(options.headers, headers);

    // add additional headers when we're using the "api key" and "api secret"
    if (this.apiSecret && this.apiKey) {
      var sig = this._generateSignature(path, method, bodyStr);

      // add signature and nonce to the header
      options.headers = assign(options.headers, {
        'S4O-API-SIGN': sig.digest,
        'S4O-API-TIMESTAMP': sig.timestamp,
        'S4O-API-KEY': this.apiKey
      })
    }

    return options;
  };

  _getHttp(path, args, callback, headers) {
    var params = '';
    if (args && !_.isEmpty(args)) {
      params = '?' + qs.stringify(args);
    }

    var url = this.baseApiUri + this._setAccessToken(path + params);
    var opts = this._generateReqOptions(url, path + params, null, 'GET', headers);

    request.get(opts, function onGet(err, response, body) {
      if (!handleHttpError(err, response, callback)) {
        if (!body) {
          callback(new Error("empty response"), null);
        } else {
          var obj = undefined;
          try {
            obj = JSON.parse(body);
          } catch (error) {
            obj = body;
          }
          callback(null, obj);
        }
      }
    });
  };

  _postHttp(path, body, callback, headers) {

    var url = this.baseApiUri + this._setAccessToken(path);
    body = body || {}

    var options = this._generateReqOptions(url, path, body, 'POST', headers);

    request.post(options, function onPost(err, response, body) {
      if (!handleHttpError(err, response, callback)) {
        if (body) {
          var obj = undefined;
          try {
            obj = JSON.parse(body);
          } catch (error) {
            obj = body;
          }
          callback(null, obj);
        } else {
          callback(null, body);
        }
      }
    });
  };

  _putHttp(path, body, callback, headers) {

    var url = this.baseApiUri + this._setAccessToken(path);

    var options = this._generateReqOptions(url, path, body, 'PUT', headers);

    request.put(options, function onPut(err, response, body) {
      if (!handleHttpError(err, response, callback)) {
        if (body) {
          var obj = undefined;
          try {
            obj = JSON.parse(body);
          } catch (error) {
            obj = body;
          }
          callback(null, obj);
        } else {
          callback(null, body);
        }
      }
    });
  };

  _deleteHttp(path, callback, headers) {
    var url = this.baseApiUri + this._setAccessToken(path);
    request.del(url, this._generateReqOptions(url, path, null, 'DELETE', headers),
      function onDel(err, response, body) {
        if (!handleHttpError(err, response, callback)) {
          callback(null, body);
        }
      });
  };
}

module.exports = ClientBase;
