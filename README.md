### install
```bash
$ npm install --save s4o-coinbase
```

### Note! This package is still in active development

The following is a list of actions that can be performed using this module
- [x] create account
- [x] account info (balance checking)
- [x] get payment address
- [x] send transaction
- [x] get transaction(s)

### constructors
Name | Parameters
-----|-----------
default | options: { 'apiKey':String, 'apiSecret':String, 'baseApiUri':String }

### methods
Name | Description | Parameters | Response
-----|-------------|------------|----------
createAccount |  | name:String, notificationUrl:String, test:Boolean | 
updateAccount |  | id:String, update:Object | 
getAccounts |  | query: Object |
getAccount |  | id:String, options:Object |
getAccounts |  | query: Object |
getAccount |  | id:String, options:Object |
generateAddress |  | account:String, currency:String, label:String (optional)
getAddresses |  | query:Object |
sendTransaction |  | account:String, currency:String, data:{ destination: amount, ... }, fee:Number, comment:String, key:String |
getTransaction |  | account:String, currency:String, hash:String |
getTransactions |  | account:String, currency:String, count:Number, skip:Number |

### usage
```javascript
const S4OClient = require('s4o-coinbase');

//initialization
const client = new S4OClient({
  baseApiUri: 'http://node_api_url',
  apiKey: '***************',
  apiSecret: '***************',
  timeout: 30000 // default timeout is 5s
});

// create account 
let createAccount = async () => {
    try {
        let response = await client.createAccount('your_domain_account1', true|false);
    } catch (e) {
        // handle exception
    }
}

// get account info
let getAccounts = async () => {
    try {
        // returns all test accounts
        let response = await client.getAccounts({ test: true });
    } catch (e) {
        // handle exception
    }
}

let getAccount = async () => {
    try {
        // fetch an account with a particular id
        let response = await client.getAccount('account_id');
    } catch (e) {
        // handle exception
    }
}

// get new payment address
let generateAddress = async () => {
    try {
        let response = await client.generateAddress('account_id', 'currency_symbol(eg BTC)', '');
    } catch (e) {
        // handle exception
    }
}

let getAddresses = async () => {
    try {
        let response = await client.getAddresses({ currency: 'BTC', account: 'account_id' });
    } catch (e) {
        // handle exception
    }
}

// send transaction
let sendTransaction = async () => {
    try {
        // Take note of currencies that don't allow multiple send eg (ETH, USDT)
        // adding only one entry will serve in such case
        let data = { 
            'address_xxxxxxxx1': 0.1,             
            ... 
        };
        let response = await client.sendTransaction('account_id', 'currency_symbol(eg BTC)', data, 0, '', '_key');
    } catch (e) {
        // handle exception
    }
}

// get transaction 
let getTransaction = async () => {
    try {
        let response = await client.getTransaction(account, currency, hash);
    } catch (e) {
        // handle exception
    }
}

// get transactions
let getTransactions = async () => {
    try {
        let response = await client.getTransactions(account, currency, count, skip);
    } catch (e) {
        // handle exception
    }
}
```